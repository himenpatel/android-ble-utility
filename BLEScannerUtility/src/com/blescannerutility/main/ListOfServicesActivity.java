package com.blescannerutility.main;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.app.ActionBar;
import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.blescannerutility.common.BluetoothDeviceActor;
import com.blescannerutility.common.ScanInterface;
import com.blescannerutility.common.Utils;
import com.blescannerutility.model.CharacteristicModel;
import com.blescannerutility.model.DescriptorModel;
import com.blescannerutility.model.ServiceModel;

public class ListOfServicesActivity extends Activity implements ScanInterface {

	private BluetoothDeviceActor bda;
	private TextView txtconnectStatus;
	private TextView txtBondStatus;
	private Menu mMenu;

	private BluetoothDevice mBluetoothdevice;
	private ArrayList<ServiceModel> servicelist;
	private ArrayList<ServiceModel> tempservicelist;
	private ArrayList<CharacteristicModel> characteristiclist;
	private ExpandableListView listview;
	public com.blescannerutility.adapter.ExpandableListAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.servicesactivity);

		initialization();

		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);

		registerReceiver(mBluetoothGattReceiver, Utils.makeIntentFilter());

		bda = new BluetoothDeviceActor(ListOfServicesActivity.this);

		connectStatus("CONNECTING...");

		mBluetoothdevice = Utils.device;

		if (mBluetoothdevice != null) {
			if (mBluetoothdevice.getBondState() == BluetoothDevice.BOND_BONDED)
				txtBondStatus.setText("BONDED");
			else
				txtBondStatus.setText("NOT BONDED");

			bda.connectedDevice(mBluetoothdevice);

			if (actionBar != null) {
				actionBar.setTitle(mBluetoothdevice.getName());
			}

		}

	}

	private void connectStatus(String connectstatus) {
		// TODO Auto-generated method stub

		txtconnectStatus.setText("Status: " + connectstatus);
	}

	private void initialization() {
		// TODO Auto-generated method stub
		txtBondStatus = (TextView) findViewById(R.id.serviceactivity_txt_bondstatus);
		txtconnectStatus = (TextView) findViewById(R.id.serviceactivity_txt_status);
		servicelist = new ArrayList<ServiceModel>();
		tempservicelist = new ArrayList<ServiceModel>();
		characteristiclist = new ArrayList<CharacteristicModel>();
		listview = (ExpandableListView) findViewById(R.id.serviceactivity_list);

		int actionbarTitleId = getResources().getIdentifier(
				"android:id/action_bar_title", null, null); // TextView hosted
															// in LinearLayout
		int upImageViewId = getResources().getIdentifier("android:id/up", null,
				null);// ImageView hosted in LinearLayout or HomeView
		int homeId = android.R.id.home;
		ImageView backView = (ImageView) findViewById(upImageViewId);
		android.widget.FrameLayout.LayoutParams param = new android.widget.FrameLayout.LayoutParams(
				android.widget.FrameLayout.LayoutParams.WRAP_CONTENT,
				android.widget.FrameLayout.LayoutParams.WRAP_CONTENT);
		param.setMargins(0, 0, 7, 0);
		backView.setLayoutParams(param);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub

		mMenu = menu;

		getMenuInflater().inflate(R.menu.services, menu);

		updateMenu();

		return true;
	}

	private void updateMenu() {
		// TODO Auto-generated method stub

		if (mMenu != null) {

			if (bda.isConnected()) {
				mMenu.findItem(R.id.disconnect).setVisible(true);
				mMenu.findItem(R.id.connect).setVisible(false);
			} else {
				mMenu.findItem(R.id.disconnect).setVisible(false);
				mMenu.findItem(R.id.connect).setVisible(true);
			}
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.connect:
			connectStatus("CONNECTING...");
			bda.connectedDevice(mBluetoothdevice);
			break;
		case R.id.disconnect:
			bda.disConnectedDevice();
			connectStatus("DISCONNECTED");
			break;
		case android.R.id.home:
			finish();
		}
		invalidateOptionsMenu();
		return true;
	}

	private BroadcastReceiver mBluetoothGattReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {

			String action = intent.getAction();

			if (action.equalsIgnoreCase(Utils.ACTION_GATT_CONNECTED)) {

				connectStatus("CONNECTED");

				updateMenu();

			} else if (action.equalsIgnoreCase(Utils.ACTION_DEVICE_NOT_FOUND)) {

			} else if (action.equalsIgnoreCase(Utils.ACTION_GATT_DISCONNECTED)) {

				connectStatus("DISCONNECTED");
				updateMenu();

			} else if (action.equalsIgnoreCase(Utils.ACTION_CONNECT_FAIL)) {
				connectStatus("DISCONNECTED");
				updateMenu();

				servicelist.clear();
				characteristiclist.clear();

			} else if (action
					.equalsIgnoreCase(Utils.ACTION_GATT_SERVICE_DISCOVERED)) {
				connectStatus("DISCOVERING SERVICES...");
			}
		}
	};

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();

		if (mBluetoothGattReceiver != null)
			unregisterReceiver(mBluetoothGattReceiver);

		if (bda != null) {
			if (bda.mBluetoothGatt != null) {
				bda.mBluetoothGatt.disconnect();
				if (bda.mBluetoothGatt != null)
					bda.mBluetoothGatt.close();
			}
		}

	}

	@Override
	public void connectedDevice(BluetoothDevice device) {
		// TODO Auto-generated method stub

	}

	@Override
	public void addDevice(BluetoothDevice scandevices, String Name, int rssi,
			Boolean isBonded) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateScannedDevice(BluetoothDevice device, int rssi) {
		// TODO Auto-generated method stub

	}

	@Override
	public void servicesList(List<BluetoothGattService> list) {
		// TODO Auto-generated method stub

		Log.e("ServiceList", "List Size = " + list.size());

		if (servicelist != null && servicelist.size() > 0)
			servicelist.clear();
		if (tempservicelist != null && tempservicelist.size() > 0)
			tempservicelist.clear();
		if (characteristiclist != null && characteristiclist.size() > 0)
			characteristiclist.clear();

		for (int i = 0; i < list.size(); i++) {

			ServiceModel model = new ServiceModel();
			String uuid = list.get(i).getUuid().toString()
					.toLowerCase(Locale.getDefault());
			String name = Utils.resolveServiceName(uuid);
			String type = (list.get(i).getType() == BluetoothGattService.SERVICE_TYPE_PRIMARY) ? "PRIMARY SERVICE"
					: "SECONDARY SERVICE";

			model.setServiceUUID(uuid);
			model.setServiceName(name);
			model.setServiceType(type);

			ArrayList<BluetoothGattCharacteristic> gattCharacteristics = (ArrayList<BluetoothGattCharacteristic>) list
					.get(i).getCharacteristics();

			ArrayList<CharacteristicModel> characteristicList = new ArrayList<CharacteristicModel>();

			for (int j = 0; j < gattCharacteristics.size(); j++) {

				CharacteristicModel chModel = new CharacteristicModel();

				String chUuid = gattCharacteristics.get(j).getUuid().toString()
						.toLowerCase(Locale.getDefault());
				String chName = Utils.resolveCharacteristicName(chUuid);
				int props = gattCharacteristics.get(j).getProperties();
				String propertiesString = "";
				if ((props & BluetoothGattCharacteristic.PROPERTY_READ) != 0)
					propertiesString += "READ,";
				if ((props & BluetoothGattCharacteristic.PROPERTY_WRITE) != 0)
					propertiesString += "WRITE,";
				if ((props & BluetoothGattCharacteristic.PROPERTY_NOTIFY) != 0)
					propertiesString += "NOTIFY,";
				if ((props & BluetoothGattCharacteristic.PROPERTY_INDICATE) != 0)
					propertiesString += "INDICATE,";
				if ((props & BluetoothGattCharacteristic.PROPERTY_WRITE_NO_RESPONSE) != 0)
					propertiesString += "WRITE_NO_RESPONSE";

				chModel.setName(chName);
				chModel.setUuid(chUuid);

				String subProperties = propertiesString.substring(0,
						propertiesString.length() - 1);

				chModel.setProperities(subProperties);

				if (((props & BluetoothGattCharacteristic.PROPERTY_WRITE) != 0)
						|| ((props & BluetoothGattCharacteristic.PROPERTY_WRITE_NO_RESPONSE) != 0))
					chModel.setWriteType("WRITE REQUEST");

				ArrayList<BluetoothGattDescriptor> gattdescriptor = (ArrayList<BluetoothGattDescriptor>) gattCharacteristics
						.get(j).getDescriptors();

				ArrayList<DescriptorModel> descriptorlist = new ArrayList<DescriptorModel>();

				for (int k = 0; k < gattdescriptor.size(); k++) {

					DescriptorModel desmodel = new DescriptorModel();

					BluetoothGattDescriptor descriptor = gattdescriptor.get(k);
					String desUuid = descriptor.getUuid().toString()
							.toLowerCase(Locale.getDefault());
					String desName = Utils.resolveDescriptorName(desUuid);

					desmodel.setDiscriptorName(desName);
					desmodel.setDiscriptorUUID(desUuid);

					descriptorlist.add(desmodel);
				}

				chModel.setDiscriptorList(descriptorlist);

				characteristicList.add(chModel);
			}

			model.setCharacteristicList(characteristicList);

			if (model.getServiceName().equalsIgnoreCase("device information")) {
				servicelist.add(model);
			} else {
				tempservicelist.add(model);
			}

		}

		if (tempservicelist != null) {
			for (int i = 0; i < tempservicelist.size(); i++) {
				servicelist.add(tempservicelist.get(i));
			}
		}

		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				adapter = new com.blescannerutility.adapter.ExpandableListAdapter(
						ListOfServicesActivity.this, servicelist, bda);
				listview.setAdapter(adapter);
				listview.setClickable(true);
				// expandAll();
			}
		});

	}

	@Override
	public void updateCharacteristicValue(final int childPosition,
			final int intValue, final String strvalue, final int groupPosition,
			final View view, final String hexvalue) {
		// TODO Auto-generated method stub

		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				adapter.updateDeviceCharacteristicValue(childPosition,
						intValue, strvalue, groupPosition, view, listview,
						hexvalue);
			}
		});

	}

	@Override
	public void updateDescriptorValue(final int childPosition,
			final int groupPosition, final String strValue, final String desUUID) {
		// TODO Auto-generated method stub

		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				adapter.updateDeviceDescriptorValue(childPosition,
						groupPosition, strValue, desUUID);
			}
		});

	}

	@Override
	public void updateDescriptorValue(final int childPosition,
			final int groupPosition, final boolean isNotify) {
		// TODO Auto-generated method stub

		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				adapter.updateDescriptorNotificationValue(childPosition,
						groupPosition, isNotify);
			}
		});

	}

	// private void expandAll() {
	// int count = adapter.getGroupCount();
	// for (int i = 0; i < count; i++) {
	// listview.expandGroup(i);
	// }
	// }

}
