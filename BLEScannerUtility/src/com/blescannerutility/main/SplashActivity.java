package com.blescannerutility.main;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.view.Window;

public class SplashActivity extends Activity implements Callback, Runnable {

	private Handler handler;
	private int splash_interval = 4000;
	private Thread thread;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.splashactivity);

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		handler = new Handler(this);
		thread = new Thread(this);
		thread.start();

	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			Thread.sleep(splash_interval);
			handler.sendEmptyMessage(0);
		} catch (InterruptedException ex) {
		}
	}

	@Override
	public boolean handleMessage(Message msg) {
		// TODO Auto-generated method stub
		if (msg.what == 0) {
			Intent intent = null;
			intent = new Intent(SplashActivity.this, ScanActivity.class);
			startActivity(intent);
			finish(); // finish the splash screen
		}
		return false;
	}

	/**
	 * called when Activity is destroy and remove the handler.
	 */
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();

		if (thread != null) {
			thread.interrupt();
			thread = null;

			if (handler != null && handler.hasMessages(0))
				handler.removeMessages(0);

		}

	}

}
