package com.blescannerutility.main;

import java.util.List;

import android.app.ActionBar;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattService;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.blescannerutility.adapter.DeviceListAdapter;
import com.blescannerutility.common.ScanDevices;
import com.blescannerutility.common.ScanInterface;
import com.blescannerutility.common.Utils;
import com.blescannerutility.model.ExtendedBluetoothDevice;

public class ScanActivity extends Activity implements ScanInterface,
		OnClickListener {

	private ListView deviceList;
	private DeviceListAdapter mAdapter;
	public static final int NO_RSSI = -1000;
	private ScanDevices scandevice;
	private TextView txtStatus;
	private TextView txtReset;
	private TextView txtSearching;
	private BluetoothAdapter btAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.scanactivity);

		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);

		Initialization();

		setListener();

		btAdapter = BluetoothAdapter.getDefaultAdapter();
		if (btAdapter.isEnabled()) {

			startScanning();
		} else {

			Utils.showBluetoothDialog("Alert",
					"Turn on Bluetooth to scan nearby devices.",
					ScanActivity.this);
		}

	}

	private void setListener() {
		// TODO Auto-generated method stub
		txtReset.setOnClickListener(this);
	}

	private void Initialization() {
		// TODO Auto-generated method stub

		deviceList = (ListView) findViewById(R.id.scanActivity_list);
		deviceList
				.setAdapter(mAdapter = new DeviceListAdapter(this, deviceList));
		txtStatus = (TextView) findViewById(R.id.scanActivity_txt_status);
		txtReset = (TextView) findViewById(R.id.scanActivity_txt_reset);
		txtSearching = (TextView) findViewById(R.id.scanActivity_txt_searching);

		int actionbarTitleId = getResources().getIdentifier(
				"android:id/action_bar_title", null, null); // TextView hosted
															// in LinearLayout
		int upImageViewId = getResources().getIdentifier("android:id/up", null,
				null);// ImageView hosted in LinearLayout or HomeView
		int homeId = android.R.id.home;
		ImageView backView = (ImageView) findViewById(upImageViewId);
		android.widget.FrameLayout.LayoutParams param = new android.widget.FrameLayout.LayoutParams(
				android.widget.FrameLayout.LayoutParams.WRAP_CONTENT,
				android.widget.FrameLayout.LayoutParams.WRAP_CONTENT);
		param.setMargins(0, 0, 7, 0);
		backView.setLayoutParams(param);

	}

	private void startScanning() {
		// TODO Auto-generated method stub

		txtStatus.setText("Status: Start Scanning");
		scandevice = new ScanDevices(ScanActivity.this);

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case Utils.REQUEST_ENABLE_BT:
			btAdapter = BluetoothAdapter.getDefaultAdapter();
			if (btAdapter != null && btAdapter.isEnabled()) {
				startScanning();
				invalidateOptionsMenu();
			} else {

				Utils.showBluetoothDialog("Alert",
						"Turn on Bluetooth to scan nearby devices.",
						ScanActivity.this);
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void connectedDevice(BluetoothDevice device) {
		// TODO Auto-generated method stub

	}

	@Override
	public void addDevice(BluetoothDevice scandevices, String Name, int rssi,
			Boolean isBonded) {
		// TODO Auto-generated method stub

		txtSearching.setVisibility(View.GONE);

		mAdapter.addOrUpdateDevice(new ExtendedBluetoothDevice(scandevices,
				Name, rssi, isBonded), scandevice);

	}

	@Override
	public void updateScannedDevice(final BluetoothDevice device, final int rssi) {
		// TODO Auto-generated method stub
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				mAdapter.updateRssiOfBondedDevice(device, rssi);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		getMenuInflater().inflate(R.menu.scanning, menu);
		if (scandevice != null && scandevice.isScanning) {
			menu.findItem(R.id.scanning_start).setVisible(false);
			menu.findItem(R.id.scanning_stop).setVisible(true);
			menu.findItem(R.id.scanning_indicator).setActionView(
					R.layout.progress_indicator);
			txtStatus.setText("Status: Start Scanning");
		} else {
			menu.findItem(R.id.scanning_start).setVisible(true);
			menu.findItem(R.id.scanning_stop).setVisible(false);
			menu.findItem(R.id.scanning_indicator).setActionView(null);
			menu.findItem(R.id.scanning_indicator).setVisible(false);
			txtStatus.setText("Status: Stop Scanning");
		}
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.scanning_start:
			txtStatus.setText("Status: Start Scanning");

			if (mAdapter != null) {

				int size = mAdapter.getdevicelistSize();
				if (size == 0)
					txtSearching.setVisibility(View.VISIBLE);
			}

			scandevice.scanLEDevice(true);
			break;
		case R.id.scanning_stop:
			txtStatus.setText("Status: Stop Scanning");
			scandevice.stopScanning();
			break;
		case android.R.id.home:
			finish();
		}
		invalidateOptionsMenu();
		return true;
	}

	@Override
	public void servicesList(List<BluetoothGattService> list) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();

		if (scandevice.isScanning)
			scandevice.stopScanning();

	}

	@Override
	public void updateCharacteristicValue(int childPosition, int intValue,
			String trim, int groupPosition, View view, final String hexvalue) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateDescriptorValue(int childPosition, int groupPosition,
			String trim, String desUUID) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateDescriptorValue(int childPosition, int groupPosition,
			boolean isNotify) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		if (v == txtReset) {

			mAdapter.resetDevice();
			scandevice.resetDeviceList();
			
			if (scandevice != null && !scandevice.isScanning) {
				if (mAdapter != null) {

					int size = mAdapter.getdevicelistSize();
					if (size == 0)
						txtSearching.setVisibility(View.VISIBLE);
				}
				scandevice.scanLEDevice(true);
				invalidateOptionsMenu();
			}
		}

	}

}
