package com.blescannerutility.adapter;

import java.util.ArrayList;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.blescannerutility.common.ScanDevices;
import com.blescannerutility.common.Utils;
import com.blescannerutility.main.ListOfServicesActivity;
import com.blescannerutility.main.R;
import com.blescannerutility.main.ScanActivity;
import com.blescannerutility.model.ExtendedBluetoothDevice;

/**
 * DeviceListAdapter class is list adapter for showing scanned Devices name,
 * address and RSSI image based on RSSI values.
 */
public class DeviceListAdapter extends BaseAdapter {

	private final ArrayList<ExtendedBluetoothDevice> mListValues = new ArrayList<ExtendedBluetoothDevice>();
	private final Context mContext;
	private final ExtendedBluetoothDevice.AddressComparator comparator = new ExtendedBluetoothDevice.AddressComparator();

	private ScanDevices scanDevice;
	private ListView list;

	public DeviceListAdapter(Context context, ListView deviceList) {
		mContext = context;
		list = deviceList;
	}

	public void updateRssiOfBondedDevice(BluetoothDevice device, int Rssi) {
		comparator.address = device.getAddress();
		final int indexInBonded = mListValues.indexOf(comparator);
		if (indexInBonded >= 0) {
			ExtendedBluetoothDevice previousDevice = mListValues
					.get(indexInBonded);
			previousDevice.rssi = Rssi;

			View v = null;
			v = list.getChildAt(indexInBonded - list.getFirstVisiblePosition());
			if (v != null) {
				ImageView imgrssi = (ImageView) v
						.findViewById(R.id.ScanActivity_list_item_rssi);
				TextView rssivalue = (TextView) v
						.findViewById(R.id.ScanActivity_list_item_Rssivalue);

				int rssi = Math.abs(previousDevice.rssi);

				if (rssi > 85) {
					imgrssi.setImageResource(R.drawable.ic_rssi_1_bar);
				} else if (rssi > 70) {
					imgrssi.setImageResource(R.drawable.ic_rssi_2_bar);
				} else if (rssi > 60) {
					imgrssi.setImageResource(R.drawable.ic_rssi_3_bar);
				} else if (rssi > 50) {
					imgrssi.setImageResource(R.drawable.ic_rssi_4_bar);
				} else {
					imgrssi.setImageResource(R.drawable.ic_rssi_5_bar);
				}
				imgrssi.setVisibility(View.VISIBLE);
				rssivalue.setText("" + previousDevice.rssi + " dBm");

			}

			// notifyDataSetChanged();
			return;
		}
	}

	public void addOrUpdateDevice(ExtendedBluetoothDevice device,
			ScanDevices scandevice) {

		mListValues.add(device);
		scanDevice = scandevice;
		notifyDataSetChanged();
	}

	public void clearDevices() {
		mListValues.clear();
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return mListValues.size();
	}

	@Override
	public Object getItem(int position) {

		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View oldView, ViewGroup parent) {
		final LayoutInflater inflater = LayoutInflater.from(mContext);

		View view = oldView;

		if (view == null) {
			view = inflater.inflate(R.layout.scanactivity_list_item, parent,
					false);
			final ViewHolder holder = new ViewHolder();
			holder.name = (TextView) view
					.findViewById(R.id.ScanActivity_list_item_deviceName);
			holder.address = (TextView) view
					.findViewById(R.id.ScanActivity_list_item_deviceAddress);
			holder.bondType = (TextView) view
					.findViewById(R.id.ScanActivity_list_item_bonded);
			holder.rssi = (ImageView) view
					.findViewById(R.id.ScanActivity_list_item_rssi);
			holder.rssivalue = (TextView) view
					.findViewById(R.id.ScanActivity_list_item_Rssivalue);
			holder.type = (TextView) view
					.findViewById(R.id.ScanActivity_list_item_typevalue);
			holder.btnConnect = (Button) view
					.findViewById(R.id.ScanActivity_list_item_btn_connect);
			view.setTag(holder);
		}

		if (mListValues != null && mListValues.size() > 0) {

			final ExtendedBluetoothDevice edxtendedDevice = mListValues
					.get(position);
			final ViewHolder holder = (ViewHolder) view.getTag();
			final String name = edxtendedDevice.name;
			holder.name.setText(name != null ? name : mContext
					.getString(R.string.not_available));
			holder.address.setText(edxtendedDevice.device.getAddress());

			if (edxtendedDevice.device.getType() == BluetoothDevice.DEVICE_TYPE_LE)
				holder.type.setText("BLE only");
			else
				holder.type.setText("UNKNOWN");

			if (edxtendedDevice.device.getBondState() == BluetoothDevice.BOND_BONDED)
				holder.bondType.setText("BONDED");
			else
				holder.bondType.setText("NOT BONDED");

			if (!edxtendedDevice.isBonded
					|| edxtendedDevice.rssi != ScanActivity.NO_RSSI) {
				// final int rssiPercent = (int) (100.0f * (127.0f +
				// edxtendedDevice.rssi) / (127.0f + 20.0f));

				int rssi = Math.abs(edxtendedDevice.rssi);

				if (rssi > 85) {
					holder.rssi.setImageResource(R.drawable.ic_rssi_1_bar);
				} else if (rssi > 70) {
					holder.rssi.setImageResource(R.drawable.ic_rssi_2_bar);
				} else if (rssi > 60) {
					holder.rssi.setImageResource(R.drawable.ic_rssi_3_bar);
				} else if (rssi > 50) {
					holder.rssi.setImageResource(R.drawable.ic_rssi_4_bar);
				} else {
					holder.rssi.setImageResource(R.drawable.ic_rssi_5_bar);
				}

				holder.rssi.setVisibility(View.VISIBLE);
				holder.rssivalue.setText("" + edxtendedDevice.rssi + " dBm");
			} else {
				holder.rssi.setVisibility(View.GONE);
			}

			holder.btnConnect.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub

					// scanDevice.stopScanning();

					Intent i = new Intent(mContext,
							ListOfServicesActivity.class);
					Utils.device = edxtendedDevice.device;
					mContext.startActivity(i);

				}
			});
		}

		return view;
	}

	private class ViewHolder {
		private TextView name;
		private TextView address;
		private TextView bondType;
		private TextView rssivalue;
		private TextView type;
		private ImageView rssi;
		private Button btnConnect;
	}

	public void resetDevice() {
		// TODO Auto-generated method stub

		mListValues.clear();
		notifyDataSetChanged();

	}

	public int getdevicelistSize() {
		// TODO Auto-generated method stub

		if (mListValues != null && mListValues.size() > 0)
			return mListValues.size();

		return 0;
	}

}
