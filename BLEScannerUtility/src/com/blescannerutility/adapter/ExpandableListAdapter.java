package com.blescannerutility.adapter;

import java.util.ArrayList;

import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseExpandableListAdapter;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.blescannerutility.common.BluetoothDeviceActor;
import com.blescannerutility.common.Utils;
import com.blescannerutility.main.R;
import com.blescannerutility.model.CharacteristicModel;
import com.blescannerutility.model.DescriptorModel;
import com.blescannerutility.model.ServiceModel;

public class ExpandableListAdapter extends BaseExpandableListAdapter {

	private Context context;
	private ArrayList<ServiceModel> list;
	private BluetoothDeviceActor BDA;
	private View convertView = null;

	public ExpandableListAdapter(Context mcontext,
			ArrayList<ServiceModel> servicelist, BluetoothDeviceActor bda) {
		// TODO Auto-generated constructor stub

		context = mcontext;
		list = servicelist;
		BDA = bda;
	}

	@Override
	public Object getChild(int groupPosition, int childPosititon) {
		if (list != null && list.size() > 0) {
			ArrayList<CharacteristicModel> characteristiclist = list.get(
					groupPosition).getCharacteristicList();
			return characteristiclist.get(childPosititon);
		}
		return null;
	}

	@Override
	public long getChildId(int groupPosition, int childPosititon) {
		// TODO Auto-generated method stub
		return childPosititon;
	}

	@Override
	public View getChildView(final int groupPosition, final int childPosition,
			boolean isLastChild, final View view, ViewGroup parent) {
		// TODO Auto-generated method stub
		convertView = view;
		final CharacteristicModel characteristics = (CharacteristicModel) getChild(
				groupPosition, childPosition);

		final ServiceModel services = (ServiceModel) getGroup(groupPosition);

		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this.context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(
					R.layout.service_expandlist_childview, null);
		}

		TextView txtchName = (TextView) convertView
				.findViewById(R.id.Service_expandlist_childview_txt_name);
		TextView txtchUUID = (TextView) convertView
				.findViewById(R.id.Service_expandlist_childview_txt_UUID);
		TextView txtchproperties = (TextView) convertView
				.findViewById(R.id.Service_expandlist_childview_txt_Properties);
		TextView txtwriteType = (TextView) convertView
				.findViewById(R.id.Service_expandlist_childview_txt_writeType);
		TextView txtdescriptor = (TextView) convertView
				.findViewById(R.id.Service_expandlist_childview_txt_Descriptor);
		TextView txtchpropertiesReadValue = (TextView) convertView
				.findViewById(R.id.Service_expandlist_childview_txt_PropertiesReadValue);
		TextView txtchpropertiesReadHexValue = (TextView) convertView
				.findViewById(R.id.Service_expandlist_childview_txt_PropertiesReadhexValue);
		LinearLayout linMain = (LinearLayout) convertView
				.findViewById(R.id.Service_expandlist_childview_lin_Descriptorlist);

		ImageView imgRead = (ImageView) convertView
				.findViewById(R.id.Service_expandlist_childview_img_read);
		ImageView imgWrite = (ImageView) convertView
				.findViewById(R.id.Service_expandlist_childview_img_write);
		ImageView imgNotify = (ImageView) convertView
				.findViewById(R.id.Service_expandlist_childview_img_notify);
		ImageView imgIndicate = (ImageView) convertView
				.findViewById(R.id.Service_expandlist_childview_img_indicate);

		linMain.removeAllViews();

		if (characteristics != null) {

			txtchName.setText(characteristics.getName());
			txtchUUID.setText("UUID: "
					+ characteristics.getUuid().toUpperCase());
			if (characteristics.getProperities() != null
					&& !characteristics.getProperities().equalsIgnoreCase("")) {
				txtchproperties.setVisibility(View.VISIBLE);
				txtchproperties.setText("Properties: "
						+ characteristics.getProperities().toUpperCase());

				if (characteristics.getProperities().contains("READ")) {
					imgRead.setVisibility(View.VISIBLE);
				} else {
					imgRead.setVisibility(View.GONE);
				}

				if (characteristics.getProperities().contains("WRITE")) {
					imgWrite.setVisibility(View.VISIBLE);
				} else {
					imgWrite.setVisibility(View.GONE);
				}

				if (characteristics.getProperities().contains("NOTIFY")) {
					imgNotify.setVisibility(View.VISIBLE);
				} else {
					imgNotify.setVisibility(View.GONE);
				}

				if (characteristics.getProperities().contains("INDICATE")) {
					imgIndicate.setVisibility(View.VISIBLE);
				} else {
					imgIndicate.setVisibility(View.GONE);
				}

			} else {
				txtchproperties.setVisibility(View.GONE);
				imgRead.setVisibility(View.GONE);
				imgWrite.setVisibility(View.GONE);
				imgNotify.setVisibility(View.GONE);
				imgIndicate.setVisibility(View.GONE);
			}

			if (characteristics.getValue() != null
					&& !characteristics.getValue().equalsIgnoreCase("")) {
				txtchpropertiesReadValue.setVisibility(View.VISIBLE);
				txtchpropertiesReadValue.setText("Value: "
						+ characteristics.getValue());
			} else {
				txtchpropertiesReadValue.setVisibility(View.GONE);
			}

			if (characteristics.getHexvalue() != null
					&& !characteristics.getHexvalue().equalsIgnoreCase("")) {
				txtchpropertiesReadHexValue.setVisibility(View.VISIBLE);
				txtchpropertiesReadHexValue.setText("Hex: 0x"
						+ characteristics.getHexvalue());
			} else {
				txtchpropertiesReadHexValue.setVisibility(View.GONE);
			}

			if (characteristics.getWriteType() != null
					&& !characteristics.getWriteType().equalsIgnoreCase("")) {
				txtwriteType.setVisibility(View.VISIBLE);
				txtwriteType.setText("Write Type: "
						+ characteristics.getWriteType().toUpperCase());
			} else {
				txtwriteType.setVisibility(View.GONE);
			}

			if (characteristics.getDiscriptorList() != null
					&& characteristics.getDiscriptorList().size() > 0) {
				txtdescriptor.setVisibility(View.VISIBLE);
				linMain.setVisibility(View.VISIBLE);

				for (int i = 0; i < characteristics.getDiscriptorList().size(); i++) {

					android.widget.RelativeLayout.LayoutParams paramrel = new android.widget.RelativeLayout.LayoutParams(
							android.widget.RelativeLayout.LayoutParams.FILL_PARENT,
							android.widget.RelativeLayout.LayoutParams.WRAP_CONTENT);

					RelativeLayout lin = new RelativeLayout(context);
					lin.setLayoutParams(paramrel);

					TextView txtDesName = new TextView(context);
					txtDesName.setTextSize(11f);
					txtDesName.setText(characteristics.getDiscriptorList()
							.get(i).getDiscriptorName());

					if (i != 0)
						txtDesName.setPadding(0, 10, 0, 0);

					lin.addView(txtDesName);

					ImageView img = new ImageView(context);

					android.widget.RelativeLayout.LayoutParams param = new android.widget.RelativeLayout.LayoutParams(
							60, 60);
					param.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
					param.setMargins(0, 0, 20, 0);
					img.setLayoutParams(param);
					img.setBackgroundResource(R.drawable.read);
					img.setPadding(10, 10, 10, 10);
					img.setId(i);
					lin.addView(img);

					linMain.addView(lin);

					TextView txtDesUUID = new TextView(context);
					txtDesUUID.setTextSize(11f);

					String requiredUUID = characteristics.getDiscriptorList()
							.get(i).getDiscriptorUUID().substring(4, 8);
					txtDesUUID.setText("UUID: 0x" + requiredUUID.toUpperCase());

					linMain.addView(txtDesUUID);

					TextView txtDesValue = new TextView(context);

					txtDesValue.setTextSize(11f);

					if (characteristics.getDiscriptorList().get(i).getValue() != null
							&& !characteristics.getDiscriptorList().get(i)
									.getValue().equalsIgnoreCase("")) {
						txtDesValue.setText("Value: "
								+ characteristics.getDiscriptorList().get(i)
										.getValue());
						txtDesValue.setVisibility(View.VISIBLE);
					} else {
						txtDesValue.setVisibility(View.GONE);
					}

					linMain.addView(txtDesValue);

					img.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View view) {
							// TODO Auto-generated method stub

							int id = view.getId();

							BDA.readDescriptor(services.getServiceUUID(),
									characteristics.getUuid(), childPosition,
									groupPosition, characteristics
											.getDiscriptorList().get(id)
											.getDiscriptorUUID());

						}
					});

				}

			} else {
				txtdescriptor.setVisibility(View.GONE);
				linMain.setVisibility(View.GONE);
			}
		}

		imgRead.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				BDA.readWriteCharcteristic(services.getServiceUUID(),
						characteristics.getUuid(), "read", childPosition,
						groupPosition, convertView, "", "");

			}
		});

		imgNotify.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				boolean isNotify = false;
				if (!characteristics.isNotifyOrIndicate())
					isNotify = true;
				else
					isNotify = false;

				BDA.notifyCahracteristic(services.getServiceUUID(),
						characteristics.getUuid(), childPosition,
						groupPosition, isNotify);

			}
		});

		imgIndicate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				boolean isNotify = false;
				if (!characteristics.isNotifyOrIndicate())
					isNotify = true;
				else
					isNotify = false;

				BDA.notifyCahracteristic(services.getServiceUUID(),
						characteristics.getUuid(), childPosition,
						groupPosition, isNotify);

			}
		});

		imgWrite.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				if (characteristics.getUuid().equalsIgnoreCase(
						"00002a00-0000-1000-8000-00805f9b34fb")) { // Generic
																	// Access
																	// for
																	// Device
																	// name

					openDeviceNameDialog(services.getServiceUUID(),
							characteristics.getUuid(), "write", childPosition,
							groupPosition);
				} else if (characteristics.getUuid().equalsIgnoreCase(
						"00002a06-0000-1000-8000-00805f9b34fb")) { // Immediate
																	// Alert for
																	// alert
																	// level
					openAlertDialog(services.getServiceUUID(),
							characteristics.getUuid(), "write", childPosition,
							groupPosition);
				} else {

					openWriteDialog(services.getServiceUUID(),
							characteristics.getUuid(), groupPosition,
							childPosition);

				}

			}
		});

		return convertView;
	}

	protected void openWriteDialog(final String serviceUUID,
			final String CharacteristicUUID, final int groupPosition,
			final int childPosition) {
		// TODO Auto-generated method stub

		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_writevalue);

		TextView txtOk = (TextView) dialog
				.findViewById(R.id.dialog_writevalue_txt_ok);
		TextView txtCancel = (TextView) dialog
				.findViewById(R.id.dialog_writevalue_txt_cancel);
		final EditText edtValue = (EditText) dialog
				.findViewById(R.id.dialog_writevalue_edt_value);

		EditText edtName = (EditText) dialog
				.findViewById(R.id.dialog_writevalue_edt_name);

		TextView txtSave = (TextView) dialog
				.findViewById(R.id.dialog_writevalue_txt_save);

		final TextView txtNew = (TextView) dialog
				.findViewById(R.id.dialog_writevalue_New);

		TextView txtLoad = (TextView) dialog
				.findViewById(R.id.dialog_writevalue_Load);

		final View viewnew = (View) dialog
				.findViewById(R.id.dialog_writevalue_view_new);

		final View viewload = (View) dialog
				.findViewById(R.id.dialog_writevalue_view_load);

		final Spinner spndatatype = (Spinner) dialog
				.findViewById(R.id.dialog_spn_datatype);

		txtNew.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				viewload.setVisibility(View.INVISIBLE);
				viewnew.setVisibility(View.VISIBLE);
			}
		});

		txtLoad.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				viewnew.setVisibility(View.INVISIBLE);
				viewload.setVisibility(View.VISIBLE);
			}
		});

		txtOk.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				if (edtValue != null
						&& !edtValue.getText().toString().equalsIgnoreCase("")) {

					if (spndatatype.getSelectedItem().toString()
							.equalsIgnoreCase(Utils.G_DATATYPE_BYTEARRAY)) {

						Log.e("Length", " "
								+ (edtValue.getText().toString().length() % 2));
						Log.e("Pattern",
								" "
										+ edtValue.getText().toString()
												.matches(Utils.regBytearray));
						if ((edtValue.getText().toString().length() % 2) == 0
								&& edtValue.getText().toString()
										.matches(Utils.regBytearray)) {
							// valid string match for reg expresstion
							String value = edtValue.getText().toString().trim();

							BDA.readWriteCharcteristic(serviceUUID,
									CharacteristicUUID, "write", childPosition,
									groupPosition, convertView, value, "bytearray");
							dialog.dismiss();
						} else {
							Toast.makeText(context,
									"Please provide valid byte array",
									Toast.LENGTH_SHORT).show();
						}

					} else if (spndatatype.getSelectedItem().toString()
							.equalsIgnoreCase(Utils.G_DATATYPE_TEXT)) {
						String value = edtValue.getText().toString().trim();

						BDA.readWriteCharcteristic(serviceUUID,
								CharacteristicUUID, "write", childPosition,
								groupPosition, convertView, value, "byte");
						dialog.dismiss();
					}

				}

			}
		});

		txtCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				dialog.dismiss();
			}
		});

		dialog.show();

	}

	protected void openAlertDialog(final String serviceUUID,
			final String charUUID, final String command,
			final int childPosition, final int groupPosition) {
		// TODO Auto-generated method stub

		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_alertlevel);

		TextView txtOk = (TextView) dialog
				.findViewById(R.id.dialog_alertlevel_txt_ok);
		TextView txtCancel = (TextView) dialog
				.findViewById(R.id.dialog_alertlevel_txt_cancel);
		final Spinner cmbAlertlevel = (Spinner) dialog
				.findViewById(R.id.dialog_alertlevel_cmb_value);

		txtOk.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				String value = (String) cmbAlertlevel.getSelectedItem();

				BDA.readWriteCharcteristic(serviceUUID, charUUID, "write",
						childPosition, groupPosition, convertView, value, "int");

				dialog.dismiss();
			}
		});

		txtCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				dialog.dismiss();
			}
		});

		dialog.show();

	}

	protected void openDeviceNameDialog(final String serviceUUID,
			final String charUUID, final String command,
			final int childPosition, final int groupPosition) {
		// TODO Auto-generated method stub

		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_devicename);

		TextView txtOk = (TextView) dialog
				.findViewById(R.id.dialog_devicename_txt_ok);
		TextView txtCancel = (TextView) dialog
				.findViewById(R.id.dialog_devicename_txt_cancel);
		final EditText edtValue = (EditText) dialog
				.findViewById(R.id.dialog_devicename_edt_value);

		txtOk.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				String value = edtValue.getText().toString().trim();

				if (value != null && !value.equalsIgnoreCase("")) {
					BDA.readWriteCharcteristic(serviceUUID, charUUID, "write",
							childPosition, groupPosition, convertView, value,
							"stringg");
				}

				dialog.dismiss();
			}
		});

		txtCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				dialog.dismiss();
			}
		});

		dialog.show();

	}

	@Override
	public int getChildrenCount(int groupPosition) {
		if (list != null && list.size() > 0) {
			ArrayList<CharacteristicModel> characteristiclist = list.get(
					groupPosition).getCharacteristicList();
			return characteristiclist.size();
		}
		return 0;
	}

	@Override
	public Object getGroup(int groupPosition) {
		// TODO Auto-generated method stub

		return list.get(groupPosition);
		// if (groupPosition < list.size())
		// return list.get(groupPosition);
		// else
		// return 0;
	}

	@Override
	public int getGroupCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		// TODO Auto-generated method stub
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		final ServiceModel continent = (ServiceModel) getGroup(groupPosition);
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this.context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.service_expandlist,
					null);
		}

		TextView txtServiceName = (TextView) convertView
				.findViewById(R.id.Service_expandlist_txt_name);
		TextView txtServiceUUID = (TextView) convertView
				.findViewById(R.id.Service_expandlist_txt_UUID);
		TextView txtServiceType = (TextView) convertView
				.findViewById(R.id.Service_expandlist_txt_type);
		ImageView imgArrowUp = (ImageView) convertView
				.findViewById(R.id.Service_expandlist_img_up);
		ImageView imgArrowDown = (ImageView) convertView
				.findViewById(R.id.Service_expandlist_img_down);

		if (continent != null) {

			txtServiceName.setText(continent.getServiceName());

			if (continent.getServiceName() != null
					&& !continent.getServiceName().equalsIgnoreCase(
							"UNKNOWN SERVICE")) {
				String requiredUUID = continent.getServiceUUID()
						.substring(4, 8);
				txtServiceUUID.setText("0x" + requiredUUID.toUpperCase());
			} else {
				txtServiceUUID
						.setText(continent.getServiceUUID().toUpperCase());
			}

			txtServiceType.setText(continent.getServiceType());

			if (isExpanded) {
				imgArrowUp.setVisibility(View.VISIBLE);
				imgArrowDown.setVisibility(View.GONE);
			} else {
				imgArrowDown.setVisibility(View.VISIBLE);
				imgArrowUp.setVisibility(View.GONE);
			}
		}

		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return true;
	}

	public void updateDeviceCharacteristicValue(int childPosition,
			int intValue, String strvalue, int groupPosition, View view1,
			ExpandableListView listview, String hexvalue) {

		ArrayList<CharacteristicModel> characteristiclist = list.get(
				groupPosition).getCharacteristicList();

		CharacteristicModel model = characteristiclist.get(childPosition);
		model.setValue(strvalue);
		if (hexvalue != null)
			model.setHexvalue(hexvalue.trim());
		notifyDataSetChanged();

	}

	public void updateDeviceDescriptorValue(int childPosition,
			int groupPosition, String strValue, String desUUID) {
		// TODO Auto-generated method stub

		ArrayList<CharacteristicModel> characteristiclist = list.get(
				groupPosition).getCharacteristicList();

		CharacteristicModel model = characteristiclist.get(childPosition);

		if (!model.isNotifyPressed()) {
			if (strValue == null || strValue.equalsIgnoreCase("")) {
				strValue = "Notifications or indications disabled";
			}
		}

		if (strValue != null && !strValue.equalsIgnoreCase("")) {

			ArrayList<DescriptorModel> deslist = model.getDiscriptorList();
			if (deslist != null && deslist.size() > 0) {
				for (int i = 0; i < deslist.size(); i++) {

					if (deslist.get(i).getDiscriptorUUID()
							.equalsIgnoreCase(desUUID)) {
						model.getDiscriptorList().get(i).setValue(strValue);
						break;
					}

				}
			}
			notifyDataSetChanged();
		}

	}

	public void updateDescriptorNotificationValue(int childPosition,
			int groupPosition, boolean isNotify) {
		// TODO Auto-generated method stub

		ArrayList<CharacteristicModel> characteristiclist = list.get(
				groupPosition).getCharacteristicList();

		CharacteristicModel model = characteristiclist.get(childPosition);

		model.setNotifyPressed(true);

		ArrayList<DescriptorModel> deslist = model.getDiscriptorList();
		if (deslist != null && deslist.size() > 0) {
			for (int i = 0; i < deslist.size(); i++) {
				if (deslist
						.get(i)
						.getDiscriptorUUID()
						.equalsIgnoreCase(
								"00002902-0000-1000-8000-00805f9b34fb")) {
					if (isNotify)
						model.getDiscriptorList()
								.get(i)
								.setValue(
										"Notifications or indications enabled");
					else
						model.getDiscriptorList()
								.get(i)
								.setValue(
										"Notifications or indications disabled");

					model.setNotifyOrIndicate(isNotify);

					break;
				}

			}
		}
		notifyDataSetChanged();

	}
}
