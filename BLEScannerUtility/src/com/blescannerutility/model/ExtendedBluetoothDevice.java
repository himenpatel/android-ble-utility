package com.blescannerutility.model;

import android.bluetooth.BluetoothDevice;

public class ExtendedBluetoothDevice {
	public BluetoothDevice device;
	public String name;
	public int rssi;
	public boolean isBonded;

	public ExtendedBluetoothDevice(BluetoothDevice device, String name, int rssi, boolean isBonded) {
		this.device = device;
		this.name = name;
		this.rssi = rssi;
		this.isBonded = isBonded;
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof ExtendedBluetoothDevice) {
			final ExtendedBluetoothDevice that = (ExtendedBluetoothDevice) o;
			return device.getAddress().equals(that.device.getAddress());
		}
		return super.equals(o);
	}

	
	public static class AddressComparator {
		public String address;

		@Override
		public boolean equals(Object o) {
			if (o instanceof ExtendedBluetoothDevice) {
				final ExtendedBluetoothDevice that = (ExtendedBluetoothDevice) o;
				return address.equals(that.device.getAddress());
			}
			return super.equals(o);
		}
	}
}
