package com.blescannerutility.model;

import java.util.ArrayList;

public class CharacteristicModel {

	String name;
	String uuid;
	String properities;
	String value;
	String writeType;
	String hexvalue = "";
	boolean isNotifyOrIndicate = false;
	boolean isNotifyPressed = false;
	
	public boolean isNotifyPressed() {
		return isNotifyPressed;
	}
	public void setNotifyPressed(boolean isNotifyPressed) {
		this.isNotifyPressed = isNotifyPressed;
	}
	public boolean isNotifyOrIndicate() {
		return isNotifyOrIndicate;
	}
	public void setNotifyOrIndicate(boolean isNotifyOrIndicate) {
		this.isNotifyOrIndicate = isNotifyOrIndicate;
	}
	public String getHexvalue() {
		return hexvalue;
	}
	public void setHexvalue(String hexvalue) {
		this.hexvalue = hexvalue;
	}
	ArrayList<DescriptorModel> discriptorList;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getProperities() {
		return properities;
	}
	public void setProperities(String properities) {
		this.properities = properities;
	}
	public String getWriteType() {
		return writeType;
	}
	public void setWriteType(String writeType) {
		this.writeType = writeType;
	}
	public ArrayList<DescriptorModel> getDiscriptorList() {
		return discriptorList;
	}
	public void setDiscriptorList(ArrayList<DescriptorModel> discriptorList) {
		this.discriptorList = discriptorList;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	
	
}
