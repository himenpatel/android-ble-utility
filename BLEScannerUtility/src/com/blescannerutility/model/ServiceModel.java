package com.blescannerutility.model;

import java.util.ArrayList;

public class ServiceModel {

	String serviceName;
	String serviceUUID;
	String serviceType;
	ArrayList<CharacteristicModel> characteristicList;

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getServiceUUID() {
		return serviceUUID;
	}

	public void setServiceUUID(String serviceUUID) {
		this.serviceUUID = serviceUUID;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public ArrayList<CharacteristicModel> getCharacteristicList() {
		return characteristicList;
	}

	public void setCharacteristicList(
			ArrayList<CharacteristicModel> characteristicList) {
		this.characteristicList = characteristicList;
	}
	
	

}
