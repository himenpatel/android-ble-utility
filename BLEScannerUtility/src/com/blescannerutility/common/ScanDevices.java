package com.blescannerutility.common;

import java.util.ArrayList;
import java.util.Timer;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import com.blescannerutility.main.ScanActivity;

@SuppressLint("NewApi")
@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
public class ScanDevices {

	private BluetoothAdapter mBluetoothAdapter;
	private ArrayList<String> scanDeviceNameList;
	private Context context;
	public ScanInterface scaninterface;
	public boolean isDeviceConnected = false;
	private static final boolean DEVICE_NOT_BONDED = false;

	public boolean isScanning = false;

	@SuppressLint("NewApi")
	public ScanDevices(ScanActivity mcontext) {
		final BluetoothManager bluetoothManager = (BluetoothManager) mcontext
				.getSystemService(Context.BLUETOOTH_SERVICE);
		mBluetoothAdapter = bluetoothManager.getAdapter();
		scanDeviceNameList = new ArrayList<String>();
		context = mcontext;
		scaninterface = mcontext;
		scanLEDevice(true);

	}

	// Scan the LE Devices.
	@SuppressLint("NewApi")
	public void scanLEDevice(boolean enable) {

		mBluetoothAdapter.startLeScan(mLeScanCallback);
		isScanning = true;

	}

	public void stopScanning() {
		try {
			mBluetoothAdapter.stopLeScan(mLeScanCallback);
			isScanning = false;

		} catch (Exception e) {
			Log.e("StopScanning", e.getMessage());
		}
	}

	// Device scan callback.
	private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {

		@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
		@Override
		public void onLeScan(final BluetoothDevice device, int rssi,
				final byte[] scanRecord) {

			if (device != null) {

				scaninterface.updateScannedDevice(device, rssi);

				if (scanDeviceNameList != null
						&& scanDeviceNameList.contains(device.getAddress())) {

				} else {
					scanDeviceNameList.add(device.getAddress());
					scaninterface.addDevice(device,
							Utils.decodeDeviceName(scanRecord), rssi,
							DEVICE_NOT_BONDED);
				}
			}
		}

	};

	public void resetDeviceList() {
		// TODO Auto-generated method stub
		scanDeviceNameList.clear();
	}

}
