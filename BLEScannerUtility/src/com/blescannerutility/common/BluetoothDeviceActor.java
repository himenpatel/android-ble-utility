package com.blescannerutility.common;

import java.lang.reflect.Method;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Queue;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;

import com.blescannerutility.main.ListOfServicesActivity;

public class BluetoothDeviceActor implements Runnable {

	private final static String TAG = BluetoothDeviceActor.class
			.getSimpleName();
	private Context mContext;
	private BluetoothManager mBluetoothManager;
	public int deviceId = 0;
	public BluetoothGatt mBluetoothGatt;
	private BluetoothDevice mDevice;
	public String deviceMacAddress;
	private boolean isConnected = false;
	private Thread thread;
	private Queue<BluetoothGattDescriptor> descriptorWriteQueue = new LinkedList<BluetoothGattDescriptor>();
	private Timer failTimer;
	private ConnectionFailerTask failerTask;
	private ScanInterface scaninterface;
	private int childPosition = -1;
	private int groupPosition = -1;
	private View view = null;
	private String desUUID = null;

	public BluetoothDeviceActor(ListOfServicesActivity context) {
		// TODO Auto-generated constructor stub
		mContext = context;
		mBluetoothManager = (BluetoothManager) context
				.getSystemService(Context.BLUETOOTH_SERVICE);
		scaninterface = context;
	}

	public int getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(int deviceId) {
		this.deviceId = deviceId;
	}

	public BluetoothGatt getmBluetoothGatt() {
		return mBluetoothGatt;
	}

	public void setmBluetoothGatt(BluetoothGatt mBluetoothGatt) {
		this.mBluetoothGatt = mBluetoothGatt;
	}

	public BluetoothDevice getmDevice() {
		return mDevice;
	}

	public void setmDevice(BluetoothDevice mDevice) {
		this.mDevice = mDevice;
	}

	public String getDeviceMacAddress() {
		return deviceMacAddress;
	}

	public void setDeviceMacAddress(String deviceMacAddress) {
		this.deviceMacAddress = deviceMacAddress;
	}

	public boolean isConnected() {
		return isConnected;
	}

	public void setConnected(boolean isConnected) {
		this.isConnected = isConnected;
	}

	public void setContext(Context context) {
		mContext = context;

	}

	public void connectedDevice(BluetoothDevice device) {
		mDevice = device;
		setDeviceMacAddress(device.getAddress());
		startThread();
	}

	public void disConnectedDevice() {

		if (mBluetoothGatt != null)
			mBluetoothGatt.disconnect();

	}

	public void goToConnect(BluetoothDevice device) {
		mDevice = device;
		setDeviceMacAddress(device.getAddress());
		startThread();
	}

	@Override
	public void run() {
		connectDevice(mDevice);
	}

	public void startThread() {
		thread = new Thread(this);
		thread.start();
	}

	public void stopThread() {
		if (thread != null) {
			final Thread tempThread = thread;
			thread = null;
			tempThread.interrupt();
		}
	}

	/**
	 * Connects to the GATT server hosted on the Bluetooth LE device.
	 * 
	 * @param device
	 *            The device address of the destination device.
	 * 
	 * @return Return true if the connection is initiated successfully. The
	 *         connection result is reported asynchronously through the
	 *         {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
	 *         callback.
	 */
	public boolean connectDevice(final BluetoothDevice device) {

		if (device == null) {
			return false;
		}

		failTimer = new Timer();
		failerTask = new ConnectionFailerTask();
		failTimer.schedule(failerTask, 10000, 50000);

		try {

			if (mContext != null) {

				mBluetoothGatt = device.connectGatt(mContext, false,
						mGattCallback);

				setmBluetoothGatt(mBluetoothGatt);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return true;
	}

	// Implements callback methods for GATT events that the app cares about. For
	// example,
	// connection change and services discovered.
	public final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {

		@Override
		public void onConnectionStateChange(BluetoothGatt gatt, int status,
				int newState) {
			super.onConnectionStateChange(gatt, status, newState);

			String intentAction;

			if (newState == BluetoothProfile.STATE_CONNECTED) {
				intentAction = Utils.ACTION_GATT_SERVICE_DISCOVERED;
				try {
					setConnected(true);
					if (failerTask != null && failTimer != null) {
						failerTask.cancel();
						failTimer.cancel();
					}
					Log.e(TAG, "Connect from GATT server.");

					broadcastUpdate(intentAction);

					stopThread();

					if (getmBluetoothGatt() != null) {
						boolean discover = getmBluetoothGatt()
								.discoverServices();
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
				Log.e(TAG, "Disconnected from GATT server.");
				setConnected(false);
				intentAction = Utils.ACTION_GATT_DISCONNECTED;
				if (getmBluetoothGatt() != null)
					refreshDeviceCache(mBluetoothGatt);
				getmBluetoothGatt().close();
				setmBluetoothGatt(null);

				if (descriptorWriteQueue != null
						&& descriptorWriteQueue.size() > 0)
					descriptorWriteQueue.clear();
				broadcastUpdate(intentAction);

			}

		}

		@Override
		public void onServicesDiscovered(BluetoothGatt gatt, int status) {
			super.onServicesDiscovered(gatt, status);

			String intentAction = Utils.ACTION_GATT_CONNECTED;
			broadcastUpdate(intentAction);
			discoverServices(gatt.getServices());

		}

		@Override
		public void onCharacteristicRead(BluetoothGatt gatt,
				BluetoothGattCharacteristic characteristic, int status) {
			// TODO Auto-generated method stub
			super.onCharacteristicRead(gatt, characteristic, status);
			Log.e(TAG, "onCharacteristicRead called");

			getCharacteristicValue(characteristic);
		}

		@Override
		public void onCharacteristicWrite(BluetoothGatt gatt,
				BluetoothGattCharacteristic characteristic, int status) {
			// TODO Auto-generated method stub
			super.onCharacteristicWrite(gatt, characteristic, status);

			Log.e(TAG, "onCharacteristicWrite called");
			getCharacteristicValue(characteristic);
		}

		@Override
		public void onCharacteristicChanged(BluetoothGatt gatt,
				BluetoothGattCharacteristic characteristic) {
			// TODO Auto-generated method stub
			super.onCharacteristicChanged(gatt, characteristic);
			Log.e(TAG, "onCharacteristicChanged called ");
			getCharacteristicValue(characteristic);

		}

		@Override
		public void onDescriptorRead(BluetoothGatt gatt,
				BluetoothGattDescriptor descriptor, int status) {
			// TODO Auto-generated method stub
			super.onDescriptorRead(gatt, descriptor, status);

			byte[] rawValue = descriptor.getValue();
			StringBuilder stringBuilder1 = null;
			try {
				stringBuilder1 = new StringBuilder(rawValue.length);
				for (byte byteChar : rawValue) {
					stringBuilder1.append(String.format("%c", byteChar));
				}

				String strValue = stringBuilder1.toString().trim();
				Log.e(TAG, "Des StrValue = " + strValue.trim());

				scaninterface.updateDescriptorValue(childPosition,
						groupPosition, strValue.trim(), desUUID);
			} catch (Exception e) {

			}
		}

		@Override
		public void onDescriptorWrite(BluetoothGatt gatt,
				BluetoothGattDescriptor descriptor, int status) {
			super.onDescriptorWrite(gatt, descriptor, status);

			descriptorWriteQueue.remove(); // pop the item that we just
			if (descriptorWriteQueue.size() > 0)
				writeGattDescriptor(descriptorWriteQueue.element());

		}

	};

	private void discoverServices(List<BluetoothGattService> list) {

		scaninterface.servicesList(list);

	}

	protected void broadcastUpdate(String intentAction) {
		// TODO Auto-generated method stub

		Intent i = new Intent();
		i.setAction(intentAction);
		mContext.sendBroadcast(i);

	}

	// Write gatt descriptor
	public void writeGattDescriptor(BluetoothGattDescriptor d) {
		d.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
		getmBluetoothGatt().writeDescriptor(d);
	}

	class ConnectionFailerTask extends TimerTask {
		@Override
		public void run() {
			broadcastUpdate(Utils.ACTION_CONNECT_FAIL);
			failerTask.cancel();
			failTimer.cancel();
		}
	}

	private boolean refreshDeviceCache(BluetoothGatt gatt) {
		try {
			BluetoothGatt localBluetoothGatt = gatt;
			Method localMethod = localBluetoothGatt.getClass().getMethod(
					"refresh", new Class[0]);
			if (localMethod != null) {
				boolean bool = ((Boolean) localMethod.invoke(
						localBluetoothGatt, new Object[0])).booleanValue();
				return bool;
			}
		} catch (Exception localException) {
			Log.e(TAG, "An exception occured while refreshing device");
		}
		return false;
	}

	/*
	 * get characteristic's value (and parse it for some types of
	 * characteristics) before calling this You should always update the value
	 * by calling requestCharacteristicValue()
	 */
	public void getCharacteristicValue(BluetoothGattCharacteristic ch) {
		if (mBluetoothGatt == null || ch == null)
			return;

		byte[] rawValue = ch.getValue();
		String strValue = null;
		String hexValue = null;
		int intValue = 0;

		if (rawValue != null) {

			// lets read and do real parsing of some characteristic to get
			// meaningful value from it
			UUID uuid = ch.getUuid();

			if (uuid.equals(Utils.Characteristic.HEART_RATE_MEASUREMENT)) { // heart
																			// rate
				// first check format used by the device - it is specified in
				// bit 0
				// and tells us if we should ask for index 1 (and uint8) or
				// index 2
				// (and uint16)
				int index = ((rawValue[0] & 0x01) == 1) ? 2 : 1;
				// also we need to define format
				int format = (index == 1) ? BluetoothGattCharacteristic.FORMAT_UINT8
						: BluetoothGattCharacteristic.FORMAT_UINT16;
				// now we have everything, get the value
				intValue = ch.getIntValue(format, index);
				strValue = intValue + " bpm"; // it is always in bpm units
			} else if (uuid.equals(Utils.Characteristic.HEART_RATE_MEASUREMENT)
					|| // manufacturer
						// name
						// string
					uuid.equals(Utils.Characteristic.MODEL_NUMBER_STRING) || // model
																				// number
																				// string)
					uuid.equals(Utils.Characteristic.FIRMWARE_REVISION_STRING)) // firmware
																				// revision
																				// string
			{

				// string value are usually simple utf8s string at index 0
				strValue = ch.getStringValue(0);
			} else if (uuid.equals(Utils.Characteristic.APPEARANCE)) { // appearance
				intValue = ((int) rawValue[1]) << 8;
				intValue += rawValue[0];
				strValue = Utils.resolveAppearance(intValue);
			} else if (uuid.equals(Utils.Characteristic.BODY_SENSOR_LOCATION)) { // body
																					// sensor
																					// location
				intValue = rawValue[0];
				strValue = Utils.resolveHeartRateSensorLocation(intValue);
			} else if (uuid.equals(Utils.Characteristic.BATTERY_LEVEL)) { // battery
																			// level
				intValue = rawValue[0];
				strValue = "" + intValue + "% battery level";
			} else {
				// not known type of characteristic, so we need to handle this
				// in
				// "general" way
				// get first four bytes and transform it to integer
				intValue = 0;
				if (rawValue.length > 0)
					intValue = (int) rawValue[0];
				if (rawValue.length > 1)
					intValue = intValue + ((int) rawValue[1] << 8);
				if (rawValue.length > 2)
					intValue = intValue + ((int) rawValue[2] << 8);
				if (rawValue.length > 3)
					intValue = intValue + ((int) rawValue[3] << 8);

				if (rawValue.length > 0) {
					StringBuilder stringBuilder = null;
					StringBuilder stringBuilder1 = null;
					try {
						stringBuilder1 = new StringBuilder(rawValue.length);
						for (byte byteChar : rawValue) {
							stringBuilder1.append(String.format("%02X",
									byteChar));
						}
						stringBuilder = new StringBuilder(rawValue.length);
						// TODO: handle exception
						for (byte byteChar : rawValue) {
							stringBuilder.append(String.format("%c", byteChar));
						}

					} catch (Exception e) {

					}
					strValue = stringBuilder.toString();
					hexValue = stringBuilder1.toString();
				}

			}

			// Log.e(TAG, "IntValue = " + intValue);
			// Log.e(TAG, "StrValue = " + strValue);

			scaninterface.updateCharacteristicValue(childPosition, intValue,
					strValue, groupPosition, view, hexValue);
		}
	}

	public void readWriteCharcteristic(String serviceUUID,
			String characteristicUuid, String action, int cposition,
			int gPosition, View convertView, String value, String datatype) {
		// TODO Auto-generated method stub

		BluetoothGattService service = null;

		UUID serviceuid = UUID.fromString(serviceUUID);

		if (serviceuid != null && getmBluetoothGatt() != null) {
			service = getmBluetoothGatt().getService(serviceuid);
		}
		UUID characteristicuid = UUID.fromString(characteristicUuid);
		BluetoothGattCharacteristic characteristic = null;
		if (service != null) {
			characteristic = service.getCharacteristic(characteristicuid);
		}
		if (characteristic != null) {

			if (mBluetoothGatt != null) {

				if (action.equalsIgnoreCase("read")) {
					boolean isread = mBluetoothGatt
							.readCharacteristic(characteristic);
					childPosition = cposition;
					groupPosition = gPosition;
					view = convertView;
					Log.e(TAG, "isread = " + isread);
				} else if (action.equalsIgnoreCase("write")) {

					childPosition = cposition;
					groupPosition = gPosition;
					view = convertView;

					int format = getValueFormat(characteristic);
					String type = Utils.resolveValueTypeDescription(format);
					Log.e(TAG, "Char Type = " + type);

					if (value != null && !value.equals("")) {

						if (datatype.equalsIgnoreCase("int")) {
							int newval = 0;
							if (value.contains("00"))
								newval = 0;
							else if (value.contains("01"))
								newval = 1;
							else if (value.contains("02"))
								newval = 2;

							byte[] byt = makeRequestByte(newval);
							characteristic.setValue(byt);
						} else if (datatype.equalsIgnoreCase("bytearray")) {

							byte[] byt = Utils.hexStringToByteArray(value);
							characteristic.setValue(byt);
						} else {

							// byte[] dataToWrite = parseHexStringToBytes(value
							// .toLowerCase(Locale.getDefault()));

							byte[] dataToWrite = value.getBytes();

							characteristic.setValue(dataToWrite);
						}

						mBluetoothGatt.writeCharacteristic(characteristic);
					}

				}
			}

		}
	}

	private byte[] makeRequestByte(int value) {

		byte[] byt = new byte[1];

		byt[0] = (byte) convertIntToHex(value);

		return byt;
	}

	public static int convertIntToHex(int n) {
		return Integer.valueOf(String.valueOf(n), 16);

	}

	public void readDescriptor(String serviceUUID, String characteristicUuid,
			int childPosition2, int groupPosition2, String discriptorUUID) {
		// TODO Auto-generated method stub
		if (getmBluetoothGatt() != null) {
			UUID serviceuid = UUID.fromString(serviceUUID);
			BluetoothGattService service = getmBluetoothGatt().getService(
					serviceuid);
			UUID characteristicuid = UUID.fromString(characteristicUuid);
			UUID desuid = UUID.fromString(discriptorUUID);
			BluetoothGattCharacteristic characteristic = null;
			if (service != null) {
				characteristic = service.getCharacteristic(characteristicuid);
			}
			BluetoothGattDescriptor descriptor = null;
			if (characteristic != null) {
				descriptor = characteristic.getDescriptor(desuid);
			}

			if (descriptor != null) {
				if (mBluetoothGatt != null) {
					boolean isread = mBluetoothGatt.readDescriptor(descriptor);
					childPosition = childPosition2;
					groupPosition = groupPosition2;
					desUUID = discriptorUUID;
				}
			}
		}
	}

	public void notifyCahracteristic(String serviceUUID,
			String characteristicUuid, int childPosition2, int groupPosition2,
			boolean isNotify) {
		// TODO Auto-generated method stub
		UUID serviceuid = UUID.fromString(serviceUUID);
		BluetoothGattService service = getmBluetoothGatt().getService(
				serviceuid);
		UUID characteristicuid = UUID.fromString(characteristicUuid);
		BluetoothGattCharacteristic characteristic = null;
		if (service != null) {
			characteristic = service.getCharacteristic(characteristicuid);

			if (mBluetoothGatt != null) {
				boolean isNotifyorNot = mBluetoothGatt
						.setCharacteristicNotification(characteristic, isNotify);
				childPosition = childPosition2;
				groupPosition = groupPosition2;

				if (isNotifyorNot) {
					scaninterface.updateDescriptorValue(childPosition,
							groupPosition, isNotify);
				}
			}
		}
	}

	public int getValueFormat(BluetoothGattCharacteristic ch) {
		int properties = ch.getProperties();

		if ((BluetoothGattCharacteristic.FORMAT_FLOAT & properties) != 0)
			return BluetoothGattCharacteristic.FORMAT_FLOAT;
		if ((BluetoothGattCharacteristic.FORMAT_SFLOAT & properties) != 0)
			return BluetoothGattCharacteristic.FORMAT_SFLOAT;
		if ((BluetoothGattCharacteristic.FORMAT_SINT16 & properties) != 0)
			return BluetoothGattCharacteristic.FORMAT_SINT16;
		if ((BluetoothGattCharacteristic.FORMAT_SINT32 & properties) != 0)
			return BluetoothGattCharacteristic.FORMAT_SINT32;
		if ((BluetoothGattCharacteristic.FORMAT_SINT8 & properties) != 0)
			return BluetoothGattCharacteristic.FORMAT_SINT8;
		if ((BluetoothGattCharacteristic.FORMAT_UINT16 & properties) != 0)
			return BluetoothGattCharacteristic.FORMAT_UINT16;
		if ((BluetoothGattCharacteristic.FORMAT_UINT32 & properties) != 0)
			return BluetoothGattCharacteristic.FORMAT_UINT32;
		if ((BluetoothGattCharacteristic.FORMAT_UINT8 & properties) != 0)
			return BluetoothGattCharacteristic.FORMAT_UINT8;

		return 0;
	}

	public byte[] parseHexStringToBytes(final String hex) {

		byte[] bytes = null;
		try {
			String tmp = hex.substring(2).replaceAll("[^[0-9][a-f]]", "");
			bytes = new byte[tmp.length() / 2];

			String part = "";

			for (int i = 0; i < bytes.length; ++i) {
				part = "0x" + tmp.substring(i * 2, i * 2 + 2);
				bytes[i] = Long.decode(part).byteValue();
			}
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return bytes;
	}
}
