package com.blescannerutility.common;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.UUID;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.SparseArray;

public class Utils {

	public final static int REQUEST_ENABLE_BT = 1001;

	public static class Service {
		final static public UUID HEART_RATE = UUID
				.fromString("0000180d-0000-1000-8000-00805f9b34fb");
	};

	public static class Characteristic {
		final static public UUID HEART_RATE_MEASUREMENT = UUID
				.fromString("00002a37-0000-1000-8000-00805f9b34fb");
		final static public UUID MANUFACTURER_STRING = UUID
				.fromString("00002a29-0000-1000-8000-00805f9b34fb");
		final static public UUID MODEL_NUMBER_STRING = UUID
				.fromString("00002a24-0000-1000-8000-00805f9b34fb");
		final static public UUID FIRMWARE_REVISION_STRING = UUID
				.fromString("00002a26-0000-1000-8000-00805f9b34fb");
		final static public UUID APPEARANCE = UUID
				.fromString("00002a01-0000-1000-8000-00805f9b34fb");
		final static public UUID BODY_SENSOR_LOCATION = UUID
				.fromString("00002a38-0000-1000-8000-00805f9b34fb");
		final static public UUID BATTERY_LEVEL = UUID
				.fromString("00002a19-0000-1000-8000-00805f9b34fb");
	}

	public static class Descriptor {
		final static public UUID CHAR_CLIENT_CONFIG = UUID
				.fromString("00002902-0000-1000-8000-00805f9b34fb");
	}

	public static String ACTION_DEVICE_NOT_FOUND = "ACTION_DEVICE_NOT_FOUND";
	public static String ACTION_CONNECT_FAIL = "ACTION_CONNECT_FAIL";
	public static String ACTION_GATT_DISCONNECTED = "ACTION_GATT_DISCONNECTED";
	public static String ACTION_GATT_CONNECTED = "ACTION_GATT_CONNECTED";
	public static String ACTION_GATT_SERVICE_DISCOVERED = "ACTION_GATT_SERVICE_DISCOVERED";
	private static HashMap<String, String> mServices = new HashMap<String, String>();
	private static HashMap<String, String> mCharacteristics = new HashMap<String, String>();
	private static HashMap<String, String> mDescriptors = new HashMap<String, String>();
	private static SparseArray<String> mAppearance = new SparseArray<String>();
	private static SparseArray<String> mHeartRateSensorLocation = new SparseArray<String>();
	private static SparseArray<String> mValueFormats = new SparseArray<String>();

	// private static final int FLAGS_BIT = 0x01;
	// private static final int SERVICES_MORE_AVAILABLE_16_BIT = 0x02;
	// private static final int SERVICES_COMPLETE_LIST_16_BIT = 0x03;
	// private static final int SERVICES_MORE_AVAILABLE_32_BIT = 0x04;
	// private static final int SERVICES_COMPLETE_LIST_32_BIT = 0x05;
	// private static final int SERVICES_MORE_AVAILABLE_128_BIT = 0x06;
	// private static final int SERVICES_COMPLETE_LIST_128_BIT = 0x07;
	private static final int SHORTENED_LOCAL_NAME = 0x08;
	private static final int COMPLETE_LOCAL_NAME = 0x09;

	// private static final byte LE_LIMITED_DISCOVERABLE_MODE = 0x01;
	// private static final byte LE_GENERAL_DISCOVERABLE_MODE = 0x02;

	public static BluetoothDevice device = null;

	public static final String G_DATATYPE_BYTEARRAY = "Byte Array";
	public static final String G_DATATYPE_TEXT = "TEXT";
	public static final String regBytearray = "^[0-9A-Fa-f]+$";

	/**
	 * Checks if device is connectable (as Android cannot get this information
	 * directly we just check if it has GENERAL DISCOVERABLE or LIMITED
	 * DISCOVERABLE flag set) and has required service UUID in the advertising
	 * packet. The service UUID may be <code>null</code>.
	 * <p>
	 * For further details on parsing BLE advertisement packet data see
	 * https://developer.bluetooth.org/Pages/default.aspx Bluetooth Core
	 * Specifications Volume 3, Part C, and Section 8
	 * </p>
	 */
	// public static boolean decodeDeviceAdvData(byte[] data, UUID requiredUUID)
	// {
	// final String uuid = requiredUUID != null ? requiredUUID.toString() :
	// null;
	// if (data != null) {
	// boolean connectable = false;
	// boolean valid = uuid == null;
	// int fieldLength, fieldName;
	// int packetLength = data.length;
	// for (int index = 0; index < packetLength; index++) {
	// fieldLength = data[index];
	// if (fieldLength == 0) {
	// return connectable && valid;
	// }
	// fieldName = data[++index];
	//
	// if (uuid != null) {
	// if (fieldName == SERVICES_MORE_AVAILABLE_16_BIT || fieldName ==
	// SERVICES_COMPLETE_LIST_16_BIT) {
	// for (int i = index + 1; i < index + fieldLength - 1; i += 2)
	// valid = valid || decodeService16BitUUID(uuid, data, i, 2);
	// } else if (fieldName == SERVICES_MORE_AVAILABLE_32_BIT || fieldName ==
	// SERVICES_COMPLETE_LIST_32_BIT) {
	// for (int i = index + 1; i < index + fieldLength - 1; i += 4)
	// valid = valid || decodeService32BitUUID(uuid, data, i, 4);
	// } else if (fieldName == SERVICES_MORE_AVAILABLE_128_BIT || fieldName ==
	// SERVICES_COMPLETE_LIST_128_BIT) {
	// for (int i = index + 1; i < index + fieldLength - 1; i += 16)
	// valid = valid || decodeService128BitUUID(uuid, data, i, 16);
	// }
	// }
	// if (fieldName == FLAGS_BIT) {
	// int flags = data[index + 1];
	// connectable = (flags & (LE_GENERAL_DISCOVERABLE_MODE |
	// LE_LIMITED_DISCOVERABLE_MODE)) > 0;
	// }
	// index += fieldLength - 1;
	// }
	// return connectable && valid;
	// }
	// return false;
	// }

	/**
	 * Decodes the device name from Complete Local Name or Shortened Local Name
	 * field in Advertisement packet. Ususally if should be done by
	 * {@link BluetoothDevice#getName()} method but some phones skips that, f.e.
	 * Sony Xperia Z1 (C6903) with Android 4.3 where getName() always returns
	 * <code>null</code>. In order to show the device name correctly we have to
	 * parse it manually :(
	 */
	public static String decodeDeviceName(byte[] data) {
		String name = null;
		int fieldLength, fieldName;
		int packetLength = data.length;
		for (int index = 0; index < packetLength; index++) {
			fieldLength = data[index];
			if (fieldLength == 0)
				break;
			fieldName = data[++index];

			if (fieldName == COMPLETE_LOCAL_NAME
					|| fieldName == SHORTENED_LOCAL_NAME) {
				name = decodeLocalName(data, index + 1, fieldLength - 1);
				break;
			}
			index += fieldLength - 1;
		}
		return name;
	}

	/**
	 * Decodes the local name
	 */
	public static String decodeLocalName(final byte[] data, final int start,
			final int length) {
		try {
			return new String(data, start, length, "UTF-8");
		} catch (final UnsupportedEncodingException e) {
			return null;
		} catch (final IndexOutOfBoundsException e) {
			return null;
		}
	}

	// /**
	// * check for required Service UUID inside device
	// */
	// private static boolean decodeService16BitUUID(String uuid, byte[] data,
	// int startPosition, int serviceDataLength) {
	// String serviceUUID = Integer.toHexString(decodeUuid16(data,
	// startPosition));
	// String requiredUUID = uuid.substring(4, 8);
	//
	// return serviceUUID.equals(requiredUUID);
	// }
	//
	// /**
	// * check for required Service UUID inside device
	// */
	// private static boolean decodeService32BitUUID(String uuid, byte[] data,
	// int startPosition, int serviceDataLength) {
	// String serviceUUID = Integer.toHexString(decodeUuid16(data, startPosition
	// + serviceDataLength - 4));
	// String requiredUUID = uuid.substring(4, 8);
	//
	// return serviceUUID.equals(requiredUUID);
	// }
	//
	// /**
	// * check for required Service UUID inside device
	// */
	// private static boolean decodeService128BitUUID(String uuid, byte[] data,
	// int startPosition, int serviceDataLength) {
	// String serviceUUID = Integer.toHexString(decodeUuid16(data, startPosition
	// + serviceDataLength - 4));
	// String requiredUUID = uuid.substring(4, 8);
	//
	// return serviceUUID.equals(requiredUUID);
	// }
	//
	// private static int decodeUuid16(final byte[] data, final int start) {
	// final int b1 = data[start] & 0xff;
	// final int b2 = data[start + 1] & 0xff;
	//
	// return (b2 << 8 | b1 << 0);
	// }

	public static IntentFilter makeIntentFilter() {

		IntentFilter filter = new IntentFilter();
		filter.addAction(ACTION_GATT_CONNECTED);
		filter.addAction(ACTION_GATT_DISCONNECTED);
		filter.addAction(ACTION_DEVICE_NOT_FOUND);
		filter.addAction(ACTION_CONNECT_FAIL);
		filter.addAction(ACTION_GATT_SERVICE_DISCOVERED);
		return filter;
	}

	static public String resolveServiceName(final String uuid) {
		String result = mServices.get(uuid);
		if (result == null)
			result = "CUSTOM SERVICE";
		return result;
	}

	static public String resolveCharacteristicName(final String uuid) {
		String result = mCharacteristics.get(uuid);
		if (result == null)
			result = "CUSTOM CHARACTERISTIC";
		return result;
	}

	static public String resolveDescriptorName(final String uuid) {
		String result = mDescriptors.get(uuid);
		if (result == null)
			result = "UNKNOWN";
		return result;
	}

	static public String resolveValueTypeDescription(final int format) {
		Integer tmp = Integer.valueOf(format);
		return mValueFormats.get(tmp, "UNKNOWN FORMAT");
	}

	static public String resolveAppearance(int key) {
		Integer tmp = Integer.valueOf(key);
		return mAppearance.get(tmp, "UNKNOWN APPEARANCE");
	}

	static public String resolveHeartRateSensorLocation(int key) {
		Integer tmp = Integer.valueOf(key);
		return mHeartRateSensorLocation.get(tmp, "OTHER");
	}

	static {
		mServices.put("00001811-0000-1000-8000-00805f9b34fb",
				"ALERT NOTIFICATION SERVICE");
		mServices
				.put("0000180f-0000-1000-8000-00805f9b34fb", "BATTERY SERVICE");
		mServices.put("00001810-0000-1000-8000-00805f9b34fb", "BLOOD PRESSURE");
		mServices.put("00001805-0000-1000-8000-00805f9b34fb",
				"CURRENT TIME SERVICE");
		mServices.put("00001818-0000-1000-8000-00805f9b34fb", "CYCLING POWER");
		mServices.put("00001816-0000-1000-8000-00805f9b34fb",
				"CYCLING SPEED AND CADENCE");
		mServices.put("0000180a-0000-1000-8000-00805f9b34fb",
				"DEVICE INFORMATION");
		mServices.put("00001800-0000-1000-8000-00805f9b34fb", "GENERIC ACCESS");
		mServices.put("00001801-0000-1000-8000-00805f9b34fb",
				"GENERIC ATTRIBUTE");
		mServices.put("00001808-0000-1000-8000-00805f9b34fb", "GLUCOSE");
		mServices.put("00001809-0000-1000-8000-00805f9b34fb",
				"HEALTH THERMOMETER");
		mServices.put("0000180d-0000-1000-8000-00805f9b34fb", "HEART RATE");
		mServices.put("00001812-0000-1000-8000-00805f9b34fb",
				"HUMAN INTERFACE DEVICE");
		mServices
				.put("00001802-0000-1000-8000-00805f9b34fb", "IMMEDIATE ALERT");
		mServices.put("00001803-0000-1000-8000-00805f9b34fb", "LINK LOSS");
		mServices.put("00001819-0000-1000-8000-00805f9b34fb",
				"LOCATION AND NAVIGATION");
		mServices.put("00001807-0000-1000-8000-00805f9b34fb",
				"NEXT DST CHANGE SERVICE");
		mServices.put("0000180e-0000-1000-8000-00805f9b34fb",
				"PHONE ALERT STATUS SERVICE");
		mServices.put("00001806-0000-1000-8000-00805f9b34fb",
				"REFERENCE TIME UPDATE SERVICE");
		mServices.put("00001814-0000-1000-8000-00805f9b34fb",
				"RUNNING SPEED AND CADENCE");
		mServices
				.put("00001813-0000-1000-8000-00805f9b34fb", "SCAN PARAMETERS");
		mServices.put("00001804-0000-1000-8000-00805f9b34fb", "TX POWER");
		// Smart Lock Services.
		mServices.put("0000180d-0000-1000-8000-00805f9b34fb",
				"HEART RATE SERVICE");
		mServices.put("00001800-0000-1000-8000-00805f9b34fb",
				"GENERIC ACCESS SERVICE");
		mServices.put("00001801-0000-1000-8000-00805f9b34fb",
				"GENERIC ATTRIBUTE SERVICE");
		mServices.put("860bf810-e3c5-11e2-a28f-0800200c9a66",
				"SMART SAFE SERVICE");
		mServices.put("9f92d9c0-e3c5-11e2-a28f-0800200c9a66",
				"USER RECORD SERVICE");
		mServices.put("918d5f80-e3c5-11e2-a28f-0800200c9a66",
				"AUDIT LOG SERVICE");
		mServices.put("b0a0d910-e3c5-11e2-a28f-0800200c9a66",
				"DEBUG LOG SERVICE");

		mCharacteristics.put("00002a43-0000-1000-8000-00805f9b34fb",
				"ALERT CATEGORY ID");
		mCharacteristics.put("00002a42-0000-1000-8000-00805f9b34fb",
				"ALERT CATEGORY ID BIT MASK");
		mCharacteristics.put("00002a06-0000-1000-8000-00805f9b34fb",
				"ALERT LEVEL");
		mCharacteristics.put("00002a44-0000-1000-8000-00805f9b34fb",
				"ALERT NOTIFICATION CONTROL POINT");
		mCharacteristics.put("00002a3f-0000-1000-8000-00805f9b34fb",
				"ALERT STATUS");
		mCharacteristics.put("00002a19-0000-1000-8000-00805f9b34fb",
				"BATTERY LEVEL");
		mCharacteristics.put("00002a49-0000-1000-8000-00805f9b34fb",
				"BLOOD PRESSURE FEATURE");
		mCharacteristics.put("00002a35-0000-1000-8000-00805f9b34fb",
				"BLOOD PRESSURE MEASUREMENT");
		mCharacteristics.put("00002a38-0000-1000-8000-00805f9b34fb",
				"BODY SENSOR LOCATION");
		mCharacteristics.put("00002a22-0000-1000-8000-00805f9b34fb",
				"BOOT KEYBOARD INPUT REPORT");
		mCharacteristics.put("00002a32-0000-1000-8000-00805f9b34fb",
				"BOOT KEYBOARD OUTPUT REPORT");
		mCharacteristics.put("00002a33-0000-1000-8000-00805f9b34fb",
				"BOOT MOUSE INPUT REPORT");
		mCharacteristics.put("00002a5c-0000-1000-8000-00805f9b34fb",
				"CSC FEATURE");
		mCharacteristics.put("00002a5b-0000-1000-8000-00805f9b34fb",
				"CSC MEASUREMENT");
		mCharacteristics.put("00002a2b-0000-1000-8000-00805f9b34fb",
				"CURRENT TIME");
		mCharacteristics.put("00002a66-0000-1000-8000-00805f9b34fb",
				"CYCLING POWER CONTROL POINT");
		mCharacteristics.put("00002a65-0000-1000-8000-00805f9b34fb",
				"CYCLING POWER FEATURE");
		mCharacteristics.put("00002a63-0000-1000-8000-00805f9b34fb",
				"CYCLING POWER MEASUREMENT");
		mCharacteristics.put("00002a64-0000-1000-8000-00805f9b34fb",
				"CYCLING POWER VECTOR");
		mCharacteristics.put("00002a08-0000-1000-8000-00805f9b34fb",
				"DATE TIME");
		mCharacteristics.put("00002a0a-0000-1000-8000-00805f9b34fb",
				"DAY DATE TIME");
		mCharacteristics.put("00002a09-0000-1000-8000-00805f9b34fb",
				"DAY OF WEEK");
		mCharacteristics.put("00002a0d-0000-1000-8000-00805f9b34fb",
				"DST OFFSET");
		mCharacteristics.put("00002a0c-0000-1000-8000-00805f9b34fb",
				"EXACT TIME 256");
		mCharacteristics.put("00002a26-0000-1000-8000-00805f9b34fb",
				"FIRMWARE REVISION STRING");
		mCharacteristics.put("00002a51-0000-1000-8000-00805f9b34fb",
				"GLUCOSE FEATURE");
		mCharacteristics.put("00002a18-0000-1000-8000-00805f9b34fb",
				"GLUCOSE MEASUREMENT");
		mCharacteristics.put("00002a34-0000-1000-8000-00805f9b34fb",
				"GLUCOSE MEASUREMENT CONTEXT");
		mCharacteristics.put("00002a27-0000-1000-8000-00805f9b34fb",
				"HARDWARE REVISION STRING");
		mCharacteristics.put("00002a39-0000-1000-8000-00805f9b34fb",
				"HEART RATE CONTROL POINT");
		mCharacteristics.put("00002a37-0000-1000-8000-00805f9b34fb",
				"HEART RATE MEASUREMENT");
		mCharacteristics.put("00002a4c-0000-1000-8000-00805f9b34fb",
				"HID CONTROL POINT");
		mCharacteristics.put("00002a4a-0000-1000-8000-00805f9b34fb",
				"HID INFORMATION");
		mCharacteristics.put("00002a2a-0000-1000-8000-00805f9b34fb",
				"IEEE 11073-20601 REGULATORY CERTIFICATION DATA LIST");
		mCharacteristics.put("00002a36-0000-1000-8000-00805f9b34fb",
				"INTERMEDIATE CUFF PRESSURE");
		mCharacteristics.put("00002a1e-0000-1000-8000-00805f9b34fb",
				"INTERMEDIATE TEMPERATURE");
		mCharacteristics.put("00002a6b-0000-1000-8000-00805f9b34fb",
				"LN CONTROL POINT");
		mCharacteristics.put("00002a6a-0000-1000-8000-00805f9b34fb",
				"LN FEATURE");
		mCharacteristics.put("00002a0f-0000-1000-8000-00805f9b34fb",
				"LOCAL TIME INFORMATION");
		mCharacteristics.put("00002a67-0000-1000-8000-00805f9b34fb",
				"LOCATION AND SPEED");
		mCharacteristics.put("00002a29-0000-1000-8000-00805f9b34fb",
				"MANUFACTURER NAME STRING");
		mCharacteristics.put("00002a21-0000-1000-8000-00805f9b34fb",
				"MEASUREMENT INTERVAL");
		mCharacteristics.put("00002a24-0000-1000-8000-00805f9b34fb",
				"MODEL NUMBER STRING");
		mCharacteristics.put("00002a68-0000-1000-8000-00805f9b34fb",
				"NAVIGATION");
		mCharacteristics.put("00002a46-0000-1000-8000-00805f9b34fb",
				"NEW ALERT");
		mCharacteristics.put("00002a50-0000-1000-8000-00805f9b34fb", "PNP ID");
		mCharacteristics.put("00002a69-0000-1000-8000-00805f9b34fb",
				"POSITION QUALITY");
		mCharacteristics.put("00002a4e-0000-1000-8000-00805f9b34fb",
				"PROTOCOL MODE");
		mCharacteristics.put("00002a52-0000-1000-8000-00805f9b34fb",
				"RECORD ACCESS CONTROL POINT");
		mCharacteristics.put("00002a14-0000-1000-8000-00805f9b34fb",
				"REFERENCE TIME INFORMATION");
		mCharacteristics.put("00002a4d-0000-1000-8000-00805f9b34fb", "REPORT");
		mCharacteristics.put("00002a4b-0000-1000-8000-00805f9b34fb",
				"REPORT MAP");
		mCharacteristics.put("00002a40-0000-1000-8000-00805f9b34fb",
				"RINGER CONTROL POINT");
		mCharacteristics.put("00002a41-0000-1000-8000-00805f9b34fb",
				"RINGER SETTING");
		mCharacteristics.put("00002a54-0000-1000-8000-00805f9b34fb",
				"RSC FEATURE");
		mCharacteristics.put("00002a53-0000-1000-8000-00805f9b34fb",
				"RSC MEASUREMENT");
		mCharacteristics.put("00002a55-0000-1000-8000-00805f9b34fb",
				"SC CONTROL POINT");
		mCharacteristics.put("00002a4f-0000-1000-8000-00805f9b34fb",
				"SCAN INTERVAL WINDOW");
		mCharacteristics.put("00002a31-0000-1000-8000-00805f9b34fb",
				"SCAN REFRESH");
		mCharacteristics.put("00002a5d-0000-1000-8000-00805f9b34fb",
				"SENSOR LOCATION");
		mCharacteristics.put("00002a25-0000-1000-8000-00805f9b34fb",
				"SERIAL NUMBER STRING");
		mCharacteristics.put("00002a05-0000-1000-8000-00805f9b34fb",
				"SERVICE CHANGED");
		mCharacteristics.put("00002a28-0000-1000-8000-00805f9b34fb",
				"SOFTWARE REVISION STRING");
		mCharacteristics.put("00002a47-0000-1000-8000-00805f9b34fb",
				"SUPPORTED NEW ALERT CATEGORY");
		mCharacteristics.put("00002a48-0000-1000-8000-00805f9b34fb",
				"SUPPORTED UNREAD ALERT CATEGORY");
		mCharacteristics.put("00002a23-0000-1000-8000-00805f9b34fb",
				"SYSTEM ID");
		mCharacteristics.put("00002a1c-0000-1000-8000-00805f9b34fb",
				"TEMPERATURE MEASUREMENT");
		mCharacteristics.put("00002a1d-0000-1000-8000-00805f9b34fb",
				"TEMPERATURE TYPE");
		mCharacteristics.put("00002a12-0000-1000-8000-00805f9b34fb",
				"TIME ACCURACY");
		mCharacteristics.put("00002a13-0000-1000-8000-00805f9b34fb",
				"TIME SOURCE");
		mCharacteristics.put("00002a16-0000-1000-8000-00805f9b34fb",
				"TIME UPDATE CONTROL POINT");
		mCharacteristics.put("00002a17-0000-1000-8000-00805f9b34fb",
				"TIME UPDATE STATE");
		mCharacteristics.put("00002a11-0000-1000-8000-00805f9b34fb",
				"TIME WITH DST");
		mCharacteristics.put("00002a0e-0000-1000-8000-00805f9b34fb",
				"TIME ZONE");
		mCharacteristics.put("00002a07-0000-1000-8000-00805f9b34fb",
				"TX POWER LEVEL");
		mCharacteristics.put("00002a45-0000-1000-8000-00805f9b34fb",
				"UNREAD ALERT STATUS");
		// USER RECORD CHARACTERISTICS
		mCharacteristics.put("9f922d01-e3c5-11e2-a28f-0800200c9a66",
				"USER COUNT");
		mCharacteristics.put("9f922d02-e3c5-11e2-a28f-0800200c9a66",
				"DEVICE USER ONE");
		// AUDIT LOG CHARACTERISTICS
		mCharacteristics.put("918d2e01-e3c5-11e2-a28f-0800200c9a66",
				"AUDIT LOG RECORDS COUNT");
		mCharacteristics.put("918d2e02-e3c5-11e2-a28f-0800200c9a66",
				"AUDIT LOG RECORD1");
		// DEBUG LOG RECORD CHARACTERISTICS
		mCharacteristics.put("b0a02f01-e3c5-11e2-a28f-0800200c9a66",
				"DEBUG LOG RECORD");
		// GENERIC ACCESS CHARACTERISTICS
		mCharacteristics.put("00002a00-0000-1000-8000-00805f9b34fb",
				"DEVICE NAME");
		mCharacteristics.put("00002a01-0000-1000-8000-00805f9b34fb",
				"APPEARANCE");
		mCharacteristics.put("00002a02-0000-1000-8000-00805f9b34fb",
				"PERIPHERAL PRIVACY FLAG");
		mCharacteristics.put("00002a03-0000-1000-8000-00805f9b34fb",
				"RECONNECTION ADDRESS");
		mCharacteristics.put("00002a04-0000-1000-8000-00805f9b34fb",
				"PERIPHERAL PREFERRED CONNECTION PARAMETERS");
		mCharacteristics.put("00002a05-0000-1000-8000-00805f9b34fb",
				"SERVICE CHANGED");

		// DISCRIPTORS

		mDescriptors.put("00002900-0000-1000-8000-00805f9b34fb",
				"Characteristic Extended Properties");
		mDescriptors.put("00002901-0000-1000-8000-00805f9b34fb",
				"Characteristic User Description");
		mDescriptors.put("00002902-0000-1000-8000-00805f9b34fb",
				"Client Characteristic Configuration");

		// lets add also couple appearance string description
		// https://developer.bluetooth.org/gatt/characteristics/Pages/CharacteristicViewer.aspx?u=org.bluetooth.characteristic.gap.appearance.xml
		mAppearance.put(Integer.valueOf(833), "HEART RATE SENSOR: BELT");
		mAppearance.put(Integer.valueOf(832), "GENERIC HEART RATE SENSOR");
		mAppearance.put(Integer.valueOf(0), "UNKNOWN");
		mAppearance.put(Integer.valueOf(64), "GENERIC PHONE");
		mAppearance.put(Integer.valueOf(1157),
				"CYCLING: SPEED AND CADENCE SENSOR");
		mAppearance.put(Integer.valueOf(1152), "GENERAL CYCLING");
		mAppearance.put(Integer.valueOf(1153), "CYCLING COMPUTER");
		mAppearance.put(Integer.valueOf(1154), "CYCLING: SPEED SENSOR");
		mAppearance.put(Integer.valueOf(1155), "CYCLING: CADENCE SENSOR");
		mAppearance.put(Integer.valueOf(1156),
				"CYCLING: SPEED AND CADENCE SENSOR");
		mAppearance.put(Integer.valueOf(1157), "CYCLING: POWER SENSOR");

		mHeartRateSensorLocation.put(Integer.valueOf(0), "OTHER");
		mHeartRateSensorLocation.put(Integer.valueOf(1), "CHEST");
		mHeartRateSensorLocation.put(Integer.valueOf(2), "WRIST");
		mHeartRateSensorLocation.put(Integer.valueOf(3), "FINGER");
		mHeartRateSensorLocation.put(Integer.valueOf(4), "HAND");
		mHeartRateSensorLocation.put(Integer.valueOf(5), "EAR LOBE");
		mHeartRateSensorLocation.put(Integer.valueOf(6), "FOOT");

		mValueFormats.put(Integer.valueOf(52), "32BIT FLOAT");
		mValueFormats.put(Integer.valueOf(50), "16BIT FLOAT");
		mValueFormats.put(Integer.valueOf(34), "16BIT SIGNED INT");
		mValueFormats.put(Integer.valueOf(36), "32BIT SIGNED INT");
		mValueFormats.put(Integer.valueOf(33), "8BIT SIGNED INT");
		mValueFormats.put(Integer.valueOf(18), "16BIT UNSIGNED INT");
		mValueFormats.put(Integer.valueOf(20), "32BIT UNSIGNED INT");
		mValueFormats.put(Integer.valueOf(17), "8BIT UNSIGNED INT");

	}

	public static void showBluetoothDialog(String title, String msg,
			final Context context) {
		AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
		dialogBuilder.setPositiveButton("Settings",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						Intent enableIntent = new Intent(
								BluetoothAdapter.ACTION_REQUEST_ENABLE);
						((Activity) context).startActivityForResult(
								enableIntent, Utils.REQUEST_ENABLE_BT);
					}
				});
		dialogBuilder.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
		dialogBuilder.setTitle(title);
		dialogBuilder.setMessage(msg);
		dialogBuilder.show();
	}

	public static byte[] hexStringToByteArray(String s) {
		int len = s.length();
		byte[] data = new byte[len / 2];
		for (int i = 0; i < len; i += 2) {
			data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character
					.digit(s.charAt(i + 1), 16));
		}
		return data;
	}

}
