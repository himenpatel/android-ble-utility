package com.blescannerutility.common;

import java.util.List;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattService;
import android.view.View;

public interface ScanInterface {

	public void connectedDevice(BluetoothDevice device);
	public void addDevice(BluetoothDevice scandevices,String Name, int rssi, Boolean isBonded);
	public void updateScannedDevice(BluetoothDevice device, int rssi);
	public void servicesList(List<BluetoothGattService> list);
	public void updateCharacteristicValue(int childPosition, int intValue,
			String trim, int groupPosition, View view, String hexValue);
	public void updateDescriptorValue(int childPosition, int groupPosition,
			String trim, String desUUID);
	public void updateDescriptorValue(int childPosition, int groupPosition,
			boolean isNotify);

}
